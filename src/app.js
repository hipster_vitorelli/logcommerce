import "dotenv/config";
import express from "express";
import path from "path";
import cors from "cors";
import bodyParser from "body-parser";
//__
import routes from "./routes";
import "./db/config";

class App {
  constructor() {
    this.server = express();
    this.middleware();
    this.routes();
  }

  middleware() {
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(
      bodyParser.json({
        type: "application/*+json",
        limit: "50mb"
      })
    );
    this.server.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    this.server.use(
      "/static",
      express.static(path.resolve(__dirname, "..", "static"))
    );
  }
  routes() {
    this.server.use(routes);
  }
}

export default new App().server;
