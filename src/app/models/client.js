import mongoose from "mongoose";
import bcrypt from "bcrypt";

const Client = new mongoose.Schema(
  {
    cliente_id: {
      type: Number
    },
    nome: {
      type: String
    },
    sobrenome: {
      type: String
    },
    data_nascimento: {
      type: Date
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true,
      index: { unique: true }
    },
    cpf: {
      type: Number
    },
    ativo: {
      type: Boolean
    },
    endereco: {
      type: Object,
      logradouro: {
        type: String
      },
      numero: {
        type: String
      },
      cep: {
        type: String
      },
      bairro: {
        type: String
      },
      estado: {
        type: String
      },
      cidade: {
        type: String
      },
      pais: {
        type: String
      }
    },
    tipo_cliente: {
      type: String
    },
    dados_juridicos: {
      type: Object
    }
  },
  {
    timestamps: true
  }
);

Client.pre("save", function(next) {
  const user = this;
  if (!user.isModified("password")) return next();

  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) return next(err);
    user.password = hash;
    next();
  });
});

export default mongoose.model("Client", Client);
