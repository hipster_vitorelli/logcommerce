import mongoose from "mongoose";
import bcrypt from "bcrypt";

const Store = new mongoose.Schema(
  {
    lojista_id: {
      type: Number
    },
    email: {
      type: String
    },
    password: {
      type: String
    },
    razao_social: {
      type: String
    },
    nome_fantasia: {
      type: String
    },
    cnpj: {
      type: String
    },
    inscricao_estadual: {
      type: String
    },
    web_site: {
      type: String
    },
    data_abertura: {
      type: Date
    },
    regime_tributario: {
      type: String
    },
    seguimento: {
      type: String
    },
    faturamento_anual: {
      type: String
    },
    endereco: {
      type: Object
    },
    pontos_de_retirada: {
      type: Array
    },
    lojista_type: {
      type: String
    },
    preco_por_km: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

Store.pre("save", function(next) {
  const user = this;
  if (!user.isModified("password")) return next();

  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) return next(err);
    user.password = hash;
    next();
  });
});

export default mongoose.model("Lojista", Store);
