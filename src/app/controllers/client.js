import Cliente from "../models/client";

class Client {
  async createUser(req, res) {
    const { email, cpf } = req.body;
    if (!email && !cpf)
      return res.json({
        error: "Alguns dados obrigatórios não foram enviados"
      });
    const search = await Cliente.findOne({ email });
    if (search)
      return res.json({ error: "Cliente já cadastrado com este e-mail" });
    try {
      const create = await Cliente.create(req.body);
      return res.json(create);
    } catch (error) {
      return res.json({ error: "Erro ao cadastrar usuário. Tente novamente" });
    }
  }

  async updateUser(req, res) {
    const search = await Cliente.findOne({ email: req.email });
    if (!search) return res.json({ error: "Cliente não encontrado na base" });
    try {
      const create = await Cliente.findOneAndUpdate(
        { email: req.email },
        req.body,
        { new: true }
      );
      return res.json(create);
    } catch (error) {
      return res.json({ error: "Erro ao cadastrar usuário. Tente novamente" });
    }
  }

  async getUser(req, res) {
    const search = await Cliente.findOne({ email: req.email });
    if (!search) return res.json({ error: "Cliente não encontrado na base" });
    try {
      const clients = await Cliente.find(
        {},
        {
          _id: 0,
          nome: 1,
          data_nascimento: 1,
          cpf: 1,
          email: 1,
          endereco: 1,
          ativo: 1
        }
      ).sort("ascending");
      return res.json(clients);
    } catch (error) {
      return res.json({ error: "Erro ao consultar usuários" });
    }
  }

  async getUserAll(req, res) {
    try {
      const clients = await Cliente.find(
        {},
        {
          _id: 0,
          nome: 1,
          data_nascimento: 1,
          cpf: 1,
          email: 1,
          endereco: 1,
          ativo: 1
        }
      ).sort("ascending");
      return res.json(clients);
    } catch (error) {
      return res.json({ error: "Erro ao consultar usuários" });
    }
  }
}

export default new Client();
