import axios from "axios";
import { formatPrice, calcShipping } from "../functions/calculoFrete";
import getStore from "../functions/store";

class Shipping {
  async createShipping(req, res) {
    const { origem, destino, email } = req.body;

    const precoKM = await getStore.getStore(email);

    const dados = await axios.get(
      `${process.env.GOOGLE_URL_DIRECTION}?origin=${origem}&destination=${destino}&optimize:true&key=${process.env.KEYMAPS}`
    );

    if (dados.data.status === "NOT_FOUND")
      return res.json({
        error:
          "Cep não encontrado. Verifique o valor digitado e tente novamente"
      });

    const frete = calcShipping(
      Number(dados.data.routes[0].legs[0].distance.value),
      Number(precoKM)
    );

    return res.json({
      rota: {
        distancia: dados.data.routes[0].legs[0].distance,
        tempo: dados.data.routes[0].legs[0].duration,
        partida: dados.data.routes[0].legs[0].start_address,
        ponto_final: dados.data.routes[0].legs[0].end_address,
        preco_km: precoKM,
        preco_frete: frete,
        preco_formatado: formatPrice(Number(frete))
      },
      status: dados.data.status
    });
  }
}

export default new Shipping();
