import jwt from "jsonwebtoken";
import sessionConfig from "../config/session";
import Client from "../models/client";
import Loja from "../models/lojista";
import bcrypt from "bcrypt";

class Session {
  async createSessionUser(req, res) {
    const { email, password } = req.body;
    if (!email || !password)
      return res.status(400).json({ error: "Verifique os dados enviados" });
    const clientSearch = await Client.findOne({ email });
    if (!clientSearch)
      return res.json({
        error: "Não encontramos nenhum cliente com este email"
      });
    const pass = bcrypt.compareSync(password, clientSearch.password);
    if (pass !== true) return res.json({ error: "Dados incorretos" });
    const token = jwt.sign({ email, password }, sessionConfig.secretMessage, {
      expiresIn: sessionConfig.expire
    });
    return res.json({ token: token, expiresIn: 7 });
  }

  async createSessionStore(req, res) {
    const { email, password } = req.body;

    if (!email || !password)
      return res.status(400).json({ error: "Verifique os dados enviados" });

    const lojaSearch = await Loja.findOne({ email });

    if (!lojaSearch)
      return res.json({
        error: "Não encontramos nenhuma Loja cadastrada com este email"
      });

    const pass = bcrypt.compareSync(password, lojaSearch.password);
    if (pass !== true) return res.json({ error: "Dados incorretos" });

    const token = jwt.sign({ email, password }, sessionConfig.secretMessage, {
      expiresIn: sessionConfig.expire
    });

    return res.json({ token: token, expiresIn: 7 });
  }
}

export default new Session();
