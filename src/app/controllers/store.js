import Lojista from "../models/lojista";

class Store {
  async getAllStores(req, res) {
    try {
      const stores = await Lojista.find().sort("ascending");
      return res.json(stores);
    } catch (error) {
      return res.json({ error: "Erro ao consultar usuários" });
    }
  }

  async getStore(req, res) {
    const { email } = req.query;
    const find = await Lojista.findOne({ email });
    if (!find) return res.status(404).json({ error: "Loja não encontrada" });
    try {
      const store = await Lojista.findOne({ email }).sort("ascending");
      return res.json(store);
    } catch (error) {
      return res.json({ error: "Erro ao consultar usuários" });
    }
  }

  async UpdateAdress(req, res) {
    const { email } = req.query;
    const find = await Lojista.findOne({ email });
    if (!find) return res.status(404).json({ error: "Loja não encontrada" });
    try {
      const newAdress = await Lojista.findOneAndUpdate(
        { email },
        {
          endereco: req.body
        },
        { new: true }
      );
      return res.json(newAdress);
    } catch (error) {
      return res.status(501).json({
        error:
          "Encontramos um erro ao se conectar com o banco de dados. Tente novamente"
      });
    }
  }

  async UpdatePickupPoints(req, res) {
    const { email } = req.query;
    const find = await Lojista.findOne({ email });
    if (!find) return res.status(404).json({ error: "Loja não encontrada" });
    try {
      const newPickupPoint = await Lojista.findOneAndUpdate(
        { email },
        {
          pontos_de_retirada: {
            $push: { compras: req.body },
            $currentDate: { lastModified: true }
          }
        },
        { new: true }
      );
      return res.json(newPickupPoint);
    } catch (error) {
      return res.status(501).json({
        error:
          "Encontramos um erro ao se conectar com o banco de dados. Tente novamente"
      });
    }
  }

  async CreateStore(req, res) {
    const find = await Lojista.findOne({ email: req.body.email });
    if (find) return res.status(404).json({ error: "Loja já cadastrada" });
    try {
      const loja = await Lojista.create(req.body);

      const { lojista_id, email } = loja;
      return res.json({
        lojista_id,
        email,
        mensagem: "Lojista criado com sucesso",
        status: true
      });
    } catch (error) {
      return res.status(501).json({
        error:
          "Encontramos um erro ao se conectar com o banco de dados. Tente novamente"
      });
    }
  }
}

export default new Store();
