function calcShipping(km, precoKM) {
  const calc = (Number(km) / 100) * Number(precoKM);
  const calcPlus = calc + calc / 3;

  //Add 10% no valor total do frete
  const porcentagem = calcPlus * 1.1;
  //Add R$ 3,00 no valor total do frete
  //const porcentagem = calc + 3;
  return (porcentagem / 10).toFixed(2);
}

function formatPrice(value) {
  return (
    "R$ " +
    value
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+\,)/g, "$1.")
  );
}

export { formatPrice, calcShipping };
