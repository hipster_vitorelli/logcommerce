import cep from "cep-promise";

class GetCEP {
  async getcep(req, res) {
    try {
      const consult = await cep(req.body.cep);
      return res.json(consult);
    } catch (error) {
      return res.json("Erro ao consultar o cep");
    }
  }
}
export default new GetCEP();
