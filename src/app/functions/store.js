import Store from "../models/lojista";

class Shipping_test {
  async getStore(email) {
    try {
      const search = await Store.findOne({ email });
      return search.preco_por_km;
    } catch (error) {
      return error;
    }
  }
}

export default new Shipping_test();
