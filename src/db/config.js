import mongoose from "mongoose";

class Database {
  constructor() {
    this.mongo();
  }
  mongo() {
    try {
      this.mongoConnection = mongoose.connect(process.env.BANCO, {
        useFindAndModify: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
      });
      console.log("**** Conexão com o banco estabelecida com sucesso");
    } catch (error) {
      console.log(error);
    }
  }
}

export default new Database();
