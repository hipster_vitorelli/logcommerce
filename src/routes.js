import { Router } from "express";
import Session from "./app/controllers/sessions";
import Client from "./app/controllers/client";
import Store from "./app/controllers/store";
import authClient from "./app/middlewares/auth_client";
import authStore from "./app/middlewares/auth_store";
import Cep from "./app/functions/getCep";
import Shipping from "./app/controllers/shipping";
const routes = Router();

routes.post("/auth/user", Session.createSessionUser);
routes.post("/auth/store", Session.createSessionStore);

routes.put("/store/adress", authStore, Store.UpdateAdress);
routes.put("/store/pickup", authStore, Store.UpdatePickupPoints);
routes.get("/store/list", authStore, Store.getStore);
routes.post("/store/create", Store.CreateStore);

routes.post("/client", Client.createUser);
routes.put("/client", authClient, Client.updateUser);
routes.get("/client", authClient, Client.getUser);

//ROTAS ADMINISTRATIVAS
routes.post("/cep", Cep.getcep);
routes.post("/shipping", Shipping.createShipping);
routes.get("/client", Client.getUserAll);
routes.get("/store/listAll", Store.getAllStores);

//ROTAS
export default routes;
